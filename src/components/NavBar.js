import React from "react";
import logo from "../assets/images/logo1.svg"
import "./styles/NavBar.css"
import Link from "react-router-dom/Link";
class NavBar extends React.Component {

  render() {
    return (
      <div className="Navbar">
        <nav className="navbar navbar-expand-lg navbar-expand-xl navbar-dark bg-darkculer fixed-top">
          <div className="container">
            <Link className="Navbar__brand" to="/home" >
              {/* <img className="Navbar__brand-logo " src={logo}
                                alt="" /> */}
              <i className="fas fa-cubes"></i>
              <span className="font-weight-light">Video   </span>
              <span className="font-weight-bold"> Movie </span>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active pull-right">
                  <a className="nav-link" href="/premiere"><i className="fas fa-home"></i>Estrenos</a>
                </li>
                <li className="nav-item pull-right">
                  <a className="nav-link" href="/favorites"><i className="fas fa-star"></i>Mis Favoritos</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>



        {/* <nav className="navbar navbar-expand-lg navbar-expand-xl navbar-dark bg-danger fixed-top">
          <div className="container">
            <a className="navbar-brand" href="/#top">React Movies</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="/#top"><i className="fas fa-home"></i> Inicio</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#premiere"><i className="fas fa-ticket-alt"></i> Estrenos</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#trend"><i className="fas fa-medal"></i> Más populares</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#favs"><i className="fas fa-star"></i> Favoritas</a>
                </li>
              </ul>
            </div>
          </div>
        </nav> */}

      </div>


    );
  }
}

export default NavBar;