import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// import Badges from '../pages/Badges';
// import BadgesNew from '../pages/BadgeNew';
import Home from '../pages/Home';
import Premiere from '../pages/Premiere';

import Layout from './Layout'
import NotFound from '../pages/NotFound'
import DetailMovie from '../pages/DetailMovie';
import Favorites from '../pages/Favorites';

function App() {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route exact path="/home" component={Home} />
                    <Route exact path="/premiere" component={Premiere} />
                    <Route
                        exact
                        path="/movie/:movieid"
                        component={DetailMovie}
                    />
                    <Route
                        exact
                        path="/favorites"
                        component={Favorites}
                    />

                    <Route component={NotFound} />
                </Switch>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
