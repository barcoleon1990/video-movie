import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './styles/MovieRow.css'

class MovieRow extends Component {

    deleteFavorites(movie) {
        let allMovies = JSON.parse(localStorage.getItem('favorite-movies')) || [];
        allMovies = allMovies.filter(function (movieFilter) { return movieFilter.id !== movie.id });
        localStorage.setItem("favorite-movies", JSON.stringify(allMovies));
        this.props.callBackDelete(movie);
        alert('Se ha eliminado ...')
    }

    render() {
        return (
            <React.Fragment>
                <div className="Movie_content col-xs-6 col-sm-5 col-md-3 col-lg-3 col-xl-3 float-left text-center" key={this.props.movie.id}>
                    <div className="row">
                        <div className="col-12">
                            <Link to={`/movie/${this.props.movie.id}`}>
                                <img className="thumb img-thumbnail " src={`https://image.tmdb.org/t/p/w500${this.props.movie.poster_path}`} />
                                <h4> {this.props.movie.title}</h4>
                                <p></p>
                            </Link>
                            {this.props.movie.isFavorite && <i className="fas fa-star favorite"></i>}
                        </div>
                        <div className="col-12">
                            {this.props.movie.showOptions && <button className="btn btn-danger" onClick={() => this.deleteFavorites(this.props.movie)}>Eliminar de mis favoritos</button>}
                        </div>
                    </div>
                </div>

            </React.Fragment>

        )
    };
}
export default MovieRow;