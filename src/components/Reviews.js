import React from 'react'
import './styles/BadgeList.css'
class Reviews extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            movie: [],
            reviews: [],
            favs: [],
            error: false
        };
    };
    render() {
        return (<ul className="list-unstyled">
            {this.props.reviews.map((review) => {
                return (
                    <li key={review.id} className="Badge__section-name-list">
                        <div className="row">
                            <div className="col content_opinion">
                                <h5>{review.author}</h5>
                                <span className='twitter__blue_font'>{review.url}</span>
                                <hr />
                                <div>{review.content}</div>

                            </div>
                        </div>
                    </li>

                );
            })}
        </ul>)
    }
}

export default Reviews