import React from 'react'
import confLogo from '../assets/images/images.png'
import './styles/NotFound.css'
import Link from 'react-router-dom/Link';

function Home() {
    return (<div>
        <div className="NotFound__hero">
            <img className="NotFound_conf-logo" src={confLogo} alt="Conf logo" />
            <Link className="btn btn-primary" to="/premiere">Encuentra tu pelicula favorita</Link>
        </div>
    </div>)
}

export default Home;