import React, { Component } from 'react';
import './styles/DetailMovie.css'
import Reviews from '../components/Reviews';
class DetailMovie extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            movie: [],
            reviews: [],
            favorites: [],
            error: false
        };
    };

    async componentDidMount() {
        try {
            const movie_id = Object.values(this.props.match.params);
            this.fetchDetailMovie(movie_id)
                .then(response => response.json())
                .then(json => {
                    let allMovies = JSON.parse(localStorage.getItem('favorite-movies')) || [];
                    let isFavorite = allMovies.find((movieFavorite) => {
                        return movieFavorite.id == movie_id
                    });
                    json.isFavorite = isFavorite ? true : false;
                    this.setState({ movie: json })
                })

            this.fetchReviews(movie_id)
                .then(response => response.json())
                .then(json => {
                    this.setState({ reviews: json.results })
                })
            this.setState({ loading: false, error: false });
        } catch (e) {
            this.setState({ loading: false, error: true })
        }
    }

    fetchDetailMovie(movie_id) {
        let url = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=fc8b3b032179ff1d503c28de0d043737&language=es-ES`;
        return fetch(url);
    };

    fetchReviews(movie_id) {
        let url = `https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=fc8b3b032179ff1d503c28de0d043737&language=es-ES`;
        return fetch(url);
    };

    saveFavs = (movie) => {
        const { favorites } = this.state;
        const favorite = {
            id: movie.id,
            title: movie.title,
            poster_path: movie.poster_path,
            overview: movie.overview,
            tagline: movie.tagline,
            vote_average: movie.vote_average
        }
        let allMovies = JSON.parse(localStorage.getItem('favorite-movies')) || [];

        let repeated = allMovies.filter(function (movie) { return movie.id == favorite.id }).length;
        if (!repeated) {
            this.setState({ favorites });
            allMovies.push(favorite);
            localStorage.setItem("favorite-movies", JSON.stringify(allMovies));
            this.componentDidMount();
            alert('Agregado a favoritos ...')
        } else { alert('Esta película ya está en tus favoritos') };
    }

    render() {
        const { movie, loading, error } = this.state;
        return (
            <React.Fragment>
                {!loading && !error && movie.id &&
                    <div className="container container-top" >
                        <div className="row" >
                            <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3  p-2 my-2 text-center">
                                <img className="imgDetail" alt="poster" src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`} />
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-8 col-lg-9 col-xl-9  p-8 my-4 text-left">
                                <h2> {movie.title}</h2>
                                <p>{movie.tagline}</p>
                                <p>Nota: {movie.vote_average}</p>
                                <p>Resumen: {movie.overview}</p>
                                {!movie.isFavorite && <button className="btn btn-primary" onClick={() => this.saveFavs(movie)}>Agregar a favoritos</button>}
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            <h2>Opiniones</h2>
                            <Reviews reviews={this.state.reviews} />
                        </div>
                    </div>
                }
                {loading &&
                    <div className="col-12 text-center">
                        <p>Loading.....</p>
                    </div>
                }
                {!loading && !error && !movie.id &&
                    <div className="col-12 text-center">
                        <h2>No hay información disponible.</h2>
                    </div>
                }
                {!loading && error &&
                    <div className="col-12 text-center">
                        <h2>Ocurrio un error.</h2>
                    </div>
                }

            </React.Fragment>
        )
    };
};
export default DetailMovie;