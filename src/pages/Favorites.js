import React, { Component } from 'react';
import MovieRow from '../components/MovieRow';
// Favs Component
class Favorites extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            movies: [],
            error: false
        };
    };

    componentDidMount() {
        const movies = localStorage.getItem("favorite-movies");
        if (movies) {
            let listMovies = JSON.parse(movies);
            listMovies.map(movie => {
                movie.showOptions = true;
            })
            this.setState({ movies: listMovies });
        }
    }

    componentWillReceiveProps() {
        const movies = localStorage.getItem("favorite-movies");
        if (movies) {
            let listMovies = JSON.parse(movies);
            listMovies.map(movie => {
                movie.showOptions = true;
            })
            this.setState({ movies: listMovies });
        }
    }

    updateState = (movie) => {
        let allMovies = this.state.movies;
        allMovies = allMovies.filter(function (movieFilter) { return movieFilter.id !== movie.id });
        this.setState({ movies: allMovies });
    };


    render() {
        const { movies, loading, error } = this.state;
        return (
            <div className="col-12 anchor" id="favs">
                <h1>Mis favoritas</h1>
                <div className="row">
                    {!loading && movies.map(movie =>
                        < MovieRow movie={movie} key={movie.id} callBackDelete={this.updateState} />
                    )}
                    {loading &&
                        <div className="col-12 text-center">
                            <p>Cargando información...</p>
                        </div>
                    }
                    {!loading && !error && !movies.length &&
                        <div className="col-12 text-center">
                            <h3>Aún no has agregado películas favoritas.</h3>
                        </div>
                    }
                    {!loading && error &&
                        <div className="col-12 text-center">
                            <h2>Ocurrió un error.</h2>
                        </div>
                    }
                </div>
            </div>
        );
    }
};
export default Favorites;