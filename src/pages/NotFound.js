import React from 'react'
import confLogo from '../assets/images/images.png'
import './styles/NotFound.css'
import Link from 'react-router-dom/Link';
function NotFound() {
    return (<div>
        <div className="NotFound__hero">
            <img className="NotFound_conf-logo" src={confLogo} alt="Conf logo" />
            No hay informacion disponible
        </div>
    </div>)
}

export default NotFound;
