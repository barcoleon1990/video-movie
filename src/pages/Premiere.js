import React from "react";
import "./styles/Premiere.css";

import MovieRow from "../components/MovieRow";


class Premiere extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            premiere: [{
                id: "420817",
                poster_path: "/1ELa9bm6zxIADz8u71VZtvNzx3k.jpg",
                title: 'Airoman',
                vote_average: '5',
                isFavorite: false
            },],
            moviesFovorites: [],
            error: false,
        };
    };

    fetchPremiere() {
        let url = 'https://api.themoviedb.org/3/discover/movie?api_key=fc8b3b032179ff1d503c28de0d043737&language=es-ES&primary_release_year=2019';
        return fetch(url);
    }

    async componentDidMount() {
        try {
            this.fetchPremiere()
                .then(response => response.json())
                .then(json => {
                    json.results.map((movie) => {
                        let allMovies = JSON.parse(localStorage.getItem('favorite-movies')) || [];
                        let isFavorite = allMovies.filter(function (movieFavorite) { return movie.id === movieFavorite.id }).length;
                        movie.isFavorite = isFavorite > 0 ? true : false;
                    })
                    this.setState({ premiere: json.results })
                })
            this.setState({ loading: false, error: false });
        } catch (e) {
            this.setState({ loading: false, error: true })
        }
    }

    render() {
        const { premiere, loading, error } = this.state;
        return (
            <React.Fragment>
                <div className="Badges__hero">
                    <div className="Badges__container">
                        !Estrenos....¡
                    </div>
                </div>
                <div className="row Movies__container ">
                    {!loading && premiere.map(movie => <MovieRow movie={movie} key={movie.id} />)}
                    {loading && <div className='col-12 text-center'> <p>Cargando información...</p> </div>}
                    {!loading && !error && !premiere.length && <div className='col-12 text-center'> <h2>No hay información disponible.</h2></div>}
                    {!loading && error && <div className='col-12 text-center'> <h2>Ocurrió un error.</h2></div>}
                </div>
            </React.Fragment>
        );
    }
}

export default Premiere;
